package empregado;

/**
 * @author Jadson Almeida
 * Created @d7 de mar de 2019
 */
public class App
{
	public static void main(String[] args)
	{
		Empregado stenyo = new EmpregadoPrimitivo("Stenyo");
		stenyo.exibirArvore();
		System.out.println("------------");
		
		Empregado lindenberg = new EmpregadoPrimitivo("Lindenberg");
		lindenberg.exibirArvore();
		System.out.println("------------");
		
		EmpregadoComposto jadson = new EmpregadoComposto("Jadson");
		jadson.addSubordinado(stenyo);
		jadson.addSubordinado(lindenberg);
		jadson.exibirArvore();
		System.out.println("------------");
		
		Empregado padreBartolomeu = new EmpregadoComposto("Padre Bartolomeu");
		((EmpregadoComposto) padreBartolomeu).addSubordinado(jadson);
		padreBartolomeu.exibirArvore();
	}
}
