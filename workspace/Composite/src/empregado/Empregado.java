package empregado;

/**
 * @author Jadson Almeida
 * Created @d7 de mar de 2019
 */
public abstract class Empregado
{
	String nome;
	
	public Empregado(String nome)
	{
		this.nome = nome;
	}

	public String getNome()
	{
		return nome;
	}

	public void setNome(String nome)
	{
		this.nome = nome;
	}
	
	public abstract void exibirArvore();
}
