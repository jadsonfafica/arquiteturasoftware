package empregado;

import java.util.ArrayList;
import java.util.List;

/** @author Jadson Almeida Created @d7 de mar de 2019 */
public class EmpregadoComposto extends Empregado
{
	List<Empregado> subordinados = new ArrayList<Empregado>();

	public EmpregadoComposto(String nome)
	{
		super(nome);
	}
	
	@Override
	public void exibirArvore()
	{
		System.out.println("Nome: " + nome);
		for (int i = 0; i < subordinados.size(); i++)
		{
			System.out.println("Subordinado " + (i + 1) + ": ");
			subordinados.get(i).exibirArvore();
		}
	}

	public void addSubordinado(Empregado subordinado)
	{
		subordinados.add(subordinado);
	}
}
