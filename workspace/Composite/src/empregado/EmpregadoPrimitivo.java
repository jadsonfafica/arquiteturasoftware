package empregado;

/**
 * @author Jadson Almeida
 * Created @d7 de mar de 2019
 */
public class EmpregadoPrimitivo extends Empregado
{
	public EmpregadoPrimitivo(String nome)
	{
		super(nome);
	}
	
	@Override
	public void exibirArvore()
	{
		System.out.println("Nome: " + nome);
	}
}
