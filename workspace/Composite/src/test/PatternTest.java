/*******************************************************************************
 * This Source Code is private for the autor's students.
 * Can't be readed to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies without autor Permission.
 * 
 * Autor: Jadson Almeida
 * Created: 26/03/2018
 ******************************************************************************/
package test;

public class PatternTest
{
	/** Cria uma 'Tela' para gravar e exibir macros */
	public static void main(String[] args)
	{
		Tela tela = new Tela();
		tela.addKeyListener(new KeyboardInput(tela.getTextArea()));
	}
}
