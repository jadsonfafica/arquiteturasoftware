/*******************************************************************************
 * This Source Code is private for the autor's students.
 * Can't be readed to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies without autor Permission.
 * 
 * Autor: Jadson Almeida
 * Created: 26/03/2018
 ******************************************************************************/
package test;

import java.awt.TextArea;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import composite.Macro;
import composite.MacroComposto;
import composite.MacroPrimitivo;

/** Recebe os inputs do teclado e controla a inser��o de macros */
public class KeyboardInput implements KeyListener
{
	/** area de texto onde serao exibidos os macros */
	private TextArea textArea;
	/** se � para registrar os inputs como sendo parte de um macro */
	private boolean criandoMacro;
	/** lista temporaria de characteres para cria��o de macro */
	private List<Character> charList;
	/** macro raiz, de onde nasce todos os demais macros */
	private Macro macroRaiz;

	/** construtor padrao, que recebe a area de texto a ser usada */
	public KeyboardInput(TextArea textArea)
	{
		this.textArea = textArea;
		charList = new ArrayList<Character>();
	}

	/** Recebe os bot�es pressionados. Verifica se � o bot�o para iniciar a grava��o de um
	 * macro ou se sao as letras a serem gravadas */
	@Override
	public void keyPressed(KeyEvent e)
	{
		if (e.getKeyCode() == KeyEvent.VK_CONTROL)
			criandoMacro = true;
		else if (criandoMacro)
			charList.add((char) e.getKeyCode());
	}

	/** Verifica se a tecla "solta" � para parar a grava��o de um macro, assim concluindo sua
	 * cria��o e exibindo-o na area de texto */
	@Override
	public void keyReleased(KeyEvent e)
	{
		if (e.getKeyCode() == KeyEvent.VK_CONTROL)
		{
			criandoMacro = false;
			if (charList.size() == 1)
			{
				if (macroRaiz == null)
					macroRaiz = new MacroPrimitivo(charList.get(0));
				else if (macroRaiz instanceof MacroPrimitivo)
				{
					char fromFolha = macroRaiz.getComando();
					macroRaiz = new MacroComposto(fromFolha);
					((MacroComposto) macroRaiz)
							.addComando(new MacroPrimitivo(charList.get(0)));
				} else
					((MacroComposto) macroRaiz)
							.addComando(new MacroPrimitivo(charList.get(0)));
			} else if (charList.size() > 1)
			{
				int index = 0;
				if (macroRaiz == null)
				{
					macroRaiz = new MacroComposto(charList.get(0));
					index = 1;
				} else if (macroRaiz instanceof MacroPrimitivo)
				{
					char fromFolha = macroRaiz.getComando();
					macroRaiz = new MacroComposto(fromFolha);
				}
				int listSize = charList.size();
				for (int i = index; i < listSize; i++)
				{
					((MacroComposto) macroRaiz)
							.addComando(new MacroPrimitivo(charList.get(i)));
				}
			} else
				return;
			// esvazia a lista
			charList.clear();
			// exibe os comandos em arvore
			macroRaiz.exibirComandos(textArea);
		}
	}

	@Override
	public void keyTyped(KeyEvent e)
	{
	}
}
