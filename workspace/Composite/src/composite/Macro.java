/*******************************************************************************
 * This Source Code is private for the autor's students.
 * Can't be readed to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies without autor Permission.
 * 
 * Autor: Jadson Almeida
 * Created: 26/03/2018
 ******************************************************************************/
package composite;

import java.awt.TextArea;

public abstract class Macro
{
	/** caractere a ser exibido */
	private char comando;

	/** recebe o caractere do macro */
	public Macro(char comando)
	{
		this.comando = comando;
	}

	public char getComando()
	{
		return comando;
	}

	/** exibe os caracteres do macro
	 * @param textArea TextArea onde ser� exibido o caractere */
	public abstract void exibirComandos(TextArea textArea);
}
