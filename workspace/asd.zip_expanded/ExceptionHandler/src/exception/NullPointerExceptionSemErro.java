/**
 * NullPointerExceptionSemErro.java
 * @author Jadson Almeida
 * Created @d12 de abr de 2018
 */
package exception;

import javax.swing.JOptionPane;

public class NullPointerExceptionSemErro
{
	public static void Mensagem(String mensagem)
	{
		JOptionPane.showMessageDialog(null, mensagem, "Erro inexperado",
				JOptionPane.ERROR_MESSAGE);
	}
}
