/**
 * Pessoa.java
 * @author Jadson Almeida
 * Created @d12 de abr de 2018
 */
package app;

import exception.NullPointerExceptionCustom;
import exception.NullPointerExceptionSemErro;

public class Pessoa
{
	private String nome;
	
	public Pessoa(String nome)
	{
		this.nome = nome;
	}
	
	public String getNome() throws NullPointerExceptionCustom
	{
		if (null == nome)
			throw new NullPointerExceptionCustom("Nome inv�lido.");
		return nome;
	}
	
	public String getNomeSemErro()
	{
		if (null == nome)
			NullPointerExceptionSemErro.Mensagem("Nome inv�lido.");
		return nome;
	}
	
	public void setNome(String nome)
	{
		this.nome = nome;
	}
}
