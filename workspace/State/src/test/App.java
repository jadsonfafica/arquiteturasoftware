package test;
import state.*;

/** @author Jadson Almeida Created @d21 de mai de 2018 */
public class App
{
	public static void main(String[] args)
	{
		Conta conta = new Conta(new ContaNormal());
		conta.depositar(8000);
		conta.depositar(8000);
		conta.depositar(8000);
		conta.sacar(10000);
		conta.sacar(10000);
		conta.sacar(10000);
		conta.sacar(10000);
		conta.depositar(10000);
	}
}
