package state;

/** @author Jadson Almeida Created @d21 de mai de 2018 Esssa � a classe CONTEXT */
public class Conta
{
	ContaEstado estadoAtual;
	private double saldo;

	public Conta(ContaEstado estadoAtual)
	{
		this.estadoAtual = estadoAtual;
	}

	public void depositar(double valor)
	{
		estadoAtual = estadoAtual.depositar(this, valor);
		System.out.println("Saldo atual: " + saldo + ". Estado: " + estadoAtual.getClass().getName());
	}

	public void sacar(double valor)
	{
		estadoAtual = estadoAtual.sacar(this, valor);
		System.out.println("Saldo atual: " + saldo + ". Estado: " + estadoAtual.getClass().getName());
	}

	public void addSaldo(double valor)
	{
		saldo += valor;
	}

	public double getSaldo()
	{
		return saldo;
	}
}
