package state;

/**
 * @author Jadson Almeida
 * Created @d21 de mai de 2018
 * Essa classe � um CONCRETE STATE
 */
public class ContaNormal implements ContaEstado
{
	@Override
	public ContaEstado depositar(Conta conta, double valor) 
	{
		 conta.addSaldo(valor);
		if (conta.getSaldo() > 10000) 
			return new ContaVip();
		return this;
	}
	
	@Override
	public ContaEstado sacar(Conta conta, double valor) 
	{
		 conta.addSaldo(-valor);;
		if (conta.getSaldo() < 0) 
			return new ContaNegativa();
		return this;
	}
}
