package state;

/** @author Jadson Almeida Created @d21 de mai de 2018 Essa classe � uma CONCRETE STATE */
public class ContaNegativa implements ContaEstado
{
	public ContaEstado depositar(Conta conta, double valor)
	{
		conta.addSaldo(valor);
		if (conta.getSaldo() > 10000)
			return new ContaVip();
		if (conta.getSaldo() > 0)
			return new ContaNormal();
		return this;
	}

	public ContaEstado sacar(Conta conta, double valor)
	{
		System.out.println("Bicha, pague meu dinheiro!" + conta.getSaldo());
		return this;
	}
}