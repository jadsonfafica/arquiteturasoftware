package state;

/** @author Jadson Almeida Created @d21 de mai de 2018 Essa � a interface STATE */
public interface ContaEstado
{
	public ContaEstado depositar(Conta conta, double valor);
	public ContaEstado sacar(Conta conta, double valor);
}
