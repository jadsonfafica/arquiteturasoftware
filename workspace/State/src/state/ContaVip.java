package state;

/** @author Jadson Almeida Created @d21 de mai de 2018 Essa � uma classe CONCRETE STATE */
public class ContaVip implements ContaEstado
{
	public ContaEstado depositar(Conta conta, double valor)
	{
		conta.addSaldo(valor);;
		sorteio(conta, valor);
		return this;
	}

	public ContaEstado sacar(Conta conta, double valor)
	{
		conta.addSaldo(-valor);
		if (conta.getSaldo() < 0)
			return new ContaNegativa();
		else if (conta.getSaldo() < 10000)
			return new ContaNormal();
		return this;
	}

	public void sorteio(Conta conta, double valor)
	{
		// sorteia um pr�mio para essa conta
		System.out.println("Sorteia pr�mio para a conta");
	}
}