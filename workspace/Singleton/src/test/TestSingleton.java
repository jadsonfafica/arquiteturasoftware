/*******************************************************************************
 * This Source Code is private for the autor's students.
 * Can't be readed to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies without autor Permission.
 * 
 * Autor: Jadson Almeida
 * Created: 26/03/2018
 ******************************************************************************/
package test;

import singleton.PlayerController;

public class TestSingleton
{
	/** imprime o id do Singleton, demonstrando que ele � �nico e imprime tamb�m a vari�vel
	 * est�tica 'abc' */
	public static void main(String[] args)
	{
		System.out.println(PlayerController.SINGLETON.GetUniqueKey());
		System.out.println(PlayerController.SINGLETON.GetUniqueKey());
		System.out.println(PlayerController.SINGLETON.GetUniqueKey());
	}
}