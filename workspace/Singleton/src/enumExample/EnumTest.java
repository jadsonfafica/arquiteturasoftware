/*******************************************************************************
 * This Source Code is private for the autor's students.
 * Can't be readed to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies without autor Permission.
 * 
 * Autor: Jadson Almeida
 * Created: 26/03/2018
 ******************************************************************************/
package enumExample;

public class EnumTest
{
	/** demonstração simples do uso de 'enum' do java */
	public static void main(String[] args)
	{
		Direcao direcao = Direcao.OESTE;
		System.out.println(direcao);
//		System.out.println(Direcao.values()[3]);
	}
}
