/*******************************************************************************
 * This Source Code is private for the autor's students.
 * Can't be readed to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies without autor Permission.
 * 
 * Autor: Jadson Almeida
 * Created: 26/03/2018
 ******************************************************************************/
package singleton;

import java.util.Random;

public enum PlayerController
{
	/** acesso unico do singleton **/
	SINGLETON;

	/** chave unica para teste do singleton **/
	private int testUniqueKey;

	/** Testa se a variavel testUniqueKey � zero. Se for zero, � gerado um inteiro aleatorio
	 * para ela. Retorna testUniqueKey **/
	public int GetUniqueKey()
	{
		if (testUniqueKey == 0)
			testUniqueKey = new Random().nextInt();
		return testUniqueKey;
	}
}
