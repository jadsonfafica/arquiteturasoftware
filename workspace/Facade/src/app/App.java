package app;

import geometria.Point3D;
import mapa.Mapa;
import missao.Missao;
import missao.Missoes;
import personagem.Personagem;
import save.JogoSalvo;

/** @author Jadson Almeida Created @d16 de abr de 2018 */
public class App
{
	public static void main(String[] args)
	{
		// Cria personagem
		Personagem personagem = new Personagem("Jadson", new Point3D(10, 15, 20));
		Mapa mapa = new Mapa();
		// Cria mapa
		mapa.addMonstro(new Point3D(1, 1, 1));
		mapa.addMonstro(new Point3D(2, 2, 2));
		mapa.addMonstro(new Point3D(55, 55, 55));
		// Cria missoes
		Missoes missoes = new Missoes();
		missoes.addMissaoCompletada(new Missao(111));
		missoes.addMissaoCompletada(new Missao(28));
		// Codigo do facade (essa � a parte relevante)
		// Salva o jogo
		JogoSalvo jogoSalvo = new JogoSalvo(personagem, missoes, mapa);
		jogoSalvo.SalvarJogo();
	}
}
