package mapa;

import java.util.ArrayList;
import java.util.List;

import geometria.Point3D;

/** @author Jadson Almeida Created @d16 de abr de 2018 */
public class Mapa
{
	List<Point3D> monstrosVivos;
	
	public Point3D[] getMonstrosVivosParaSalvar()
	{
		int size = monstrosVivos.size();
		Point3D[] listaDeIndex = new Point3D[size]; 
		for (int i = 0; i < size; i++)
		{
			listaDeIndex[i] = monstrosVivos.get(i);
		}
		System.out.println("lista de monstros vivos para salvar, gerada.");
		return listaDeIndex;
	}

	public void addMonstro(Point3D monstro)
	{
		if (null == monstrosVivos)
			monstrosVivos = new ArrayList<Point3D>();
		monstrosVivos.add(monstro);
	}

	public List<Point3D> getMonstrosVivos()
	{
		return monstrosVivos;
	}

	public void setMonstrosVivos(List<Point3D> monstrosVivos)
	{
		this.monstrosVivos = monstrosVivos;
	}
}
