package missao;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Jadson Almeida
 * Created @d16 de abr de 2018
 */
public class Missoes
{
	private List<Missao> missoesCompletadas;

	public int[] getMissoesCompletadasParaSalvar()
	{
		int size = missoesCompletadas.size();
		int[] listaDeIndex = new int[size]; 
		for (int i = 0; i < size; i++)
		{
			listaDeIndex[i] = missoesCompletadas.get(i).getIndex();
		}
		System.out.println("lista de missoes completadas para salvar, gerada.");
		return listaDeIndex;
	}
	
	public void addMissaoCompletada(Missao missao)
	{
		if (null == missoesCompletadas)
			missoesCompletadas = new ArrayList<Missao>();
		missoesCompletadas.add(missao);
	}
	
	public List<Missao> getMissoesCompletadas()
	{
		return missoesCompletadas;
	}

	public void setMissoesCompletadas(List<Missao> missoesCompletadas)
	{
		this.missoesCompletadas = missoesCompletadas;
	}
}
