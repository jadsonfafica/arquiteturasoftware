package missao;

/** @author Jadson Almeida Created @d16 de abr de 2018 */
public class Missao
{
	private int index;

	public Missao(int index)
	{
		this.index = index;
	}

	public int getIndex()
	{
		return index;
	}

	public void setIndex(int index)
	{
		this.index = index;
	}
}