package save;

import geometria.Point3D;
import mapa.Mapa;
import missao.Missoes;
import personagem.Personagem;

/** @author Jadson Almeida Created @d16 de abr de 2018. Classe facada, que unifica a forma de
 *         salvar o jogo */
public class JogoSalvo
{
	private Personagem personagem;
	private Missoes missoes;
	private Mapa mapa;

	public JogoSalvo(Personagem personagem, Missoes missoes, Mapa mapa)
	{
		this.personagem = personagem;
		this.missoes = missoes;
		this.mapa = mapa;
	}

	/** Executa todas as 3 opera��es para salvar o jogo (personagem, missoes, mapa), e no fim
	 * salva o arquivo */
	public void SalvarJogo()
	{
		String p = personagem.codigoSalvarPersonagem();
		int[] mi = missoes.getMissoesCompletadasParaSalvar();
		Point3D[] ma = mapa.getMonstrosVivosParaSalvar();
		String arquivo = p + "," + mi + "," + ma;
		System.out.println(arquivo);
		// encripta e salva o arquivo
	}

	public void CarregarJogoSalvo(String nomeDoArquivo)
	{
		// carrega o jogo com o arquivo indicado
	}
}
