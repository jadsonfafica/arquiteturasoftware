package personagem;

import geometria.Point3D;

/** @author Jadson Almeida Created @d16 de abr de 2018 */
public class Personagem
{
	private String nome;
	private Point3D posicao;

	public Personagem(String nome, Point3D posicao)
	{
		this.nome = nome;
		this.posicao = posicao;
	}
	
	public String codigoSalvarPersonagem()
	{
		System.out.println("Codigo para salvar personagem gerado.");
		return (nome + posicao.getX() + "," + posicao.getY() + "," + posicao.getZ());
	}

	// Getters and Setters
	public String getNome()
	{
		return nome;
	}

	public void setNome(String nome)
	{
		this.nome = nome;
	}

	public Point3D getPosicao()
	{
		return posicao;
	}

	public void setPosicao(Point3D posicao)
	{
		this.posicao = posicao;
	}
}
