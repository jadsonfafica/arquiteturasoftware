package observer;

/**
 * @author Jadson Almeida
 * Created @d7 de mai de 2018
 */
public class Hater implements Observer
{
	private Idolo idolo;

	public Hater(Idolo idolo)
	{
		this.idolo = idolo;
	}

	@Override
	public void update()
	{
		System.out.println(
				"O lixo do " + idolo.getNome() + " postou " + idolo.getNovoPost() + "... Mercenário!!!");
	}
}
