package observer;

/**
 * @author Jadson Almeida
 * Created @d7 de mai de 2018
 */
public interface Observable
{
	public void attach(Observer observer);
	public void detach(Observer observer);
	public void notifyObservers();
}
