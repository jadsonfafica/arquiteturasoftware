package observer;

/**
 * @author Jadson Almeida
 * Created @d7 de mai de 2018
 */
public interface Observer
{
	public void update();
}
