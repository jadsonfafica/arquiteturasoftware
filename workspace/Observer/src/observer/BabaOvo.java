package observer;

/** @author Jadson Almeida Created @d7 de mai de 2018 */
public class BabaOvo implements Observer
{
	private Idolo idolo;

	public BabaOvo(Idolo idolo)
	{
		this.idolo = idolo;
	}

	@Override
	public void update()
	{
		System.out.println(
				"Ai, " + idolo.getNome() + " postou " + idolo.getNovoPost() + " S2 S2 S2");
	}

}
