package observer;

import java.util.ArrayList;
import java.util.List;

/** @author Jadson Almeida Created @d7 de mai de 2018 */
public class Idolo implements Observable
{
	private String nome;
	private String novoPost;
	private List<Observer> observers;

	public Idolo(String nome)
	{
		this.nome = nome;
	}

	public void postarNoInstagram(String post)
	{
		novoPost = post;
		System.out.println(nome + " postou: " + post);
		notifyObservers();
	}

	public String getNome()
	{
		return nome;
	}

	public String getNovoPost()
	{
		return novoPost;
	}

	@Override
	public void attach(Observer observer)
	{
		if (null == observers)
			observers = new ArrayList<Observer>();
		observers.add(observer);
	}

	@Override
	public void detach(Observer observer)
	{
		if (null == observers)
			return;
		observers.remove(observer);
	}

	@Override
	public void notifyObservers()
	{
		if (null == observers)
			return;
		for (Observer observer : observers)
		{
			observer.update();
		}
	}
}
