package app;

import observer.BabaOvo;
import observer.Hater;
import observer.Idolo;

/** @author Jadson Almeida Created @d7 de mai de 2018 */
public class Test
{
	public static void main(String[] args)
	{
		Idolo neymar = new Idolo("Neymar");
		BabaOvo babaOvo = new BabaOvo(neymar);
		Hater hater = new Hater(neymar);
		neymar.attach(babaOvo);
		neymar.attach(hater);
		neymar.postarNoInstagram("Hoje eu vou jogar PUBG!");
	}
}
