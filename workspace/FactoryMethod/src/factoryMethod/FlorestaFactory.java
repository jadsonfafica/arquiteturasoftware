/*******************************************************************************
 * This Source Code is private for the autor's students.
 * Can't be readed to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies without autor Permission.
 * 
 * Autor: Jadson Almeida
 * Created: 26/03/2018
 ******************************************************************************/
package factoryMethod;

/** Fabrica de arvores da flora floresta */
public class FlorestaFactory extends ArvoreFactory
{
	@Override
	public void criarArvores()
	{
		arvores.add(new Cerejeira());
		arvores.add(new Pinheiro());
	}
}
