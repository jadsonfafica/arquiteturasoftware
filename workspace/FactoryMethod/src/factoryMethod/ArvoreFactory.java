/*******************************************************************************
 * This Source Code is private for the autor's students.
 * Can't be readed to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies without autor Permission.
 * 
 * Autor: Jadson Almeida
 * Created: 26/03/2018
 ******************************************************************************/
package factoryMethod;

import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public abstract class ArvoreFactory
{
	/** lista de arvores de uma determinada flora */
	List<Arvore> arvores = new ArrayList<Arvore>();

	/** cria as arvores de uma determinada flora */
	public abstract void criarArvores();

	/** cria um frame e exibe as arvores contidas na variavel 'arvores' */
	public void exibirArvores()
	{
		// cria um frame (tipo de janela)
		JFrame frame = new JFrame();
		// define que ao fechar a janela, a aplica��o tamb�m � fechada
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		for (int i = 0; i < arvores.size(); i++)
		{
			// atribui o estilo do layout para 'Flow'
			frame.getContentPane().setLayout(new FlowLayout());
			// adiciona a imagem da arvore como label/icone no frame
			frame.add(new JLabel(new ImageIcon(arvores.get(i).getImagem())));
		}
		// ajusta o tamanho do frame para o conte�do dentro dele (imagem)
		frame.pack();
		// exibe o frame
		frame.setVisible(true);
	}
}
