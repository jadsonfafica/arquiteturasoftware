/*******************************************************************************
 * This Source Code is private for the autor's students.
 * Can't be readed to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies without autor Permission.
 * 
 * Autor: Jadson Almeida
 * Created: 26/03/2018
 ******************************************************************************/
package factoryMethod;

/** Arvore do tipo Cerejeira */
public class Cerejeira extends Arvore
{
	public Cerejeira()
	{
		super("cerejeira.jpg");
	}
}
