/*******************************************************************************
 * This Source Code is private for the autor's students.
 * Can't be readed to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies without autor Permission.
 * 
 * Autor: Jadson Almeida
 * Created: 26/03/2018
 ******************************************************************************/
package test;

import factoryMethod.ArvoreFactory;
import factoryMethod.DesertoFactory;

public class TestFactoryMethod
{
	/** cria uma f�brica de arvores (florestas), gera as arvores do tipo da fabrica e as
	 * exibe */
	public static void main(String[] args)
	{
		ArvoreFactory fabricaDeArvore = new DesertoFactory();
		fabricaDeArvore.criarArvores();
		fabricaDeArvore.exibirArvores();
	}
}
