/*******************************************************************************
 * This Source Code is private for the autor's students.
 * Can't be readed to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies without autor Permission.
 * 
 * Autor: Jadson Almeida
 * Created: 
 ******************************************************************************/
package Prototype;

/** Represents the Monsters attributes */
public class Monster
{
	/** unique key of each monster clone */
	private long id;
	/** name or title of the monster */
	private String name;
	/** attack value of the monster */
	private byte attack;
	/** defense value of the monster */
	private byte defense;
	/** movement speed value of the monster */
	private byte speed;
	/** health points value of the monster */
	private short hp;

	/** Regular constructor */
	public Monster(long id, String name, byte attack, byte defense, byte speed, short hp)
	{
		this.id = id;
		this.name = name;
		this.attack = attack;
		this.defense = defense;
		this.speed = speed;
		this.hp = hp;
	}

	/** Clone the original Monster
	 * @param original The original prototype to be cloned */
	public Monster(Monster original)
	{
		this(original.getId(), original.getName(), original.getAttack(), original.getDefense(),
				original.getSpeed(), original.getHp());
	}

	// Gets & Setters
	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public byte getAttack()
	{
		return attack;
	}

	public void setAttack(byte attack)
	{
		this.attack = attack;
	}

	public byte getDefense()
	{
		return defense;
	}

	public void setDefense(byte defense)
	{
		this.defense = defense;
	}

	public byte getSpeed()
	{
		return speed;
	}

	public void setSpeed(byte speed)
	{
		this.speed = speed;
	}

	public short getHp()
	{
		return hp;
	}

	public void setHp(short hp)
	{
		this.hp = hp;
	}

}
