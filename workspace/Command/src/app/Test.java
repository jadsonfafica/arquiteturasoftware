package app;

import command.*;

/** @author Jadson Almeida Created @d14 de mai de 2018 */
public class Test
{
	public static void main(String[] args)
	{
		Personagem p = new Personagem();
		Joystick joystick = new Joystick();
		ComandoJogador idle = new Idle(p);
		joystick.setComando(0, idle, idle);
		joystick.setComando(1, new Pular(p), new CancelarPulo(p));
		joystick.setComando(2, new Atirar(p), new CessarTiro(p));
		joystick.setComando(3, new Agachar(p), new Levantar(p));
		joystick.setComando(4, new TrocarArma(p), new VoltarArma(p));
		joystick.apertarBotao(2);
		joystick.soltarBotao(2);
		joystick.apertarBotao(3);
		joystick.soltarBotao(3);
		
		System.out.println("\nTestando com botoes diferentes para fazer/desfazer:");		
		ComandoJogador nada = new Nada();
		joystick.setComando(5, new Agachar(p), nada);
		joystick.setComando(6, new Levantar(p), nada);
		joystick.apertarBotao(5);
		joystick.soltarBotao(5);
		joystick.apertarBotao(6);
		joystick.soltarBotao(6);
		
		System.out.println("\nTestando com o mesmo botao clicado novamente para desfazer:");
		joystick.setComando(7, new AgacharToggle(p), nada);
		joystick.apertarBotao(7);
		joystick.soltarBotao(7);
		System.out.println("2 segundos depois...");
		joystick.apertarBotao(7);
		joystick.soltarBotao(7);
		joystick.apertarBotao(7);
		joystick.soltarBotao(7);
		joystick.apertarBotao(7);
		joystick.soltarBotao(7);
	}
}
