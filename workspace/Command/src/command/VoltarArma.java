package command;

/**
 * @author Jadson Almeida
 * Created @d14 de mai de 2018
 */
public class VoltarArma implements ComandoJogador
{
	private Personagem personagem;
	
	public VoltarArma(Personagem personagem)
	{
		this.personagem = personagem;
	}
	
	@Override
	public void executar()
	{
		personagem.voltarArma();
	}

}
