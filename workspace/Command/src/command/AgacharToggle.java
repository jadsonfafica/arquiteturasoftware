package command;

/** @author Jadson Almeida Created @d14 de mai de 2018 */
public class AgacharToggle implements ComandoJogador
{
	private Personagem personagem;
	private boolean agachado;

	public AgacharToggle(Personagem personagem)
	{
		this.personagem = personagem;
	}

	@Override
	public void executar()
	{
		if (agachado)
			personagem.levantar();
		else
			personagem.agachar();
		agachado ^= true;
	}

}
