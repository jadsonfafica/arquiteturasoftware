package command;

/**
 * @author Jadson Almeida
 * Created @d14 de mai de 2018
 */
public class Agachar implements ComandoJogador
{
	private Personagem personagem;
	
	public Agachar(Personagem personagem)
	{
		this.personagem = personagem;
	}
	
	@Override
	public void executar()
	{
		personagem.agachar();
	}

}
