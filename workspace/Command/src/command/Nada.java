package command;

/**
 * @author Jadson Almeida
 * Created @d14 de mai de 2018
 * Essa classe � um "NoCommand"
 */
public class Nada implements ComandoJogador
{
	@Override
	public void executar()
	{
		// vazio
	}

}
