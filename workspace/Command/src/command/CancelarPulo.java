package command;

/** @author Jadson Almeida Created @d14 de mai de 2018 */
public class CancelarPulo implements ComandoJogador
{
	private Personagem personagem;

	public CancelarPulo(Personagem personagem)
	{
		this.personagem = personagem;
	}

	@Override
	public void executar()
	{
		personagem.cancelaPulo();
	}

}
