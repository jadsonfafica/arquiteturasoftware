package command;

/**
 * @author Jadson Almeida
 * Created @d14 de mai de 2018
 */
public class Levantar implements ComandoJogador
{
	private Personagem personagem;
	
	public Levantar(Personagem personagem)
	{
		this.personagem = personagem;
	}
	
	@Override
	public void executar()
	{
		personagem.levantar();
	}

}
