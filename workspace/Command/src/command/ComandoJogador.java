package command;

/**
 * @author Jadson Almeida
 * Created @d14 de mai de 2018
 */
public interface ComandoJogador
{
	public void executar();
}
