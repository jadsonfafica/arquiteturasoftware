package command;

/**
 * @author Jadson Almeida
 * Created @d14 de mai de 2018
 */
public class Atirar implements ComandoJogador
{
	private Personagem personagem;
	
	public Atirar(Personagem personagem)
	{
		this.personagem = personagem;
	}
	
	@Override
	public void executar()
	{
		personagem.atirar();
	}

}
