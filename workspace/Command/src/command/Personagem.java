package command;

/** @author Jadson Almeida Created @d14 de mai de 2018 Essa � a classe Receiver */
public class Personagem
{
	public void idle()
	{
		System.out.println("anima��o de �cio");
	}

	public void pular()
	{
		System.out.println("pula sobe at� um limite = mario");
	}

	public void cancelaPulo()
	{
		System.out.println("para de subir");
	}

	public void atirar()
	{
		System.out.println("atira com a arma initerruptamente");
	}

	public void cessarTiro()
	{
		System.out.println("para de atirar");
	}

	public void agachar()
	{
		System.out.println("se agacha");
	}

	public void levantar()
	{
		System.out.println("fica em p�");
	}

	public void proximaArma()
	{
		System.out.println("empunha a pr�xima arma");
	}

	public void voltarArma()
	{
		System.out.println("empunha a arma anterior");
	}

}
