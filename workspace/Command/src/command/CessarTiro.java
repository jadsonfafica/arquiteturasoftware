package command;

/**
 * @author Jadson Almeida
 * Created @d14 de mai de 2018
 */
public class CessarTiro implements ComandoJogador
{
	private Personagem personagem;
	
	public CessarTiro(Personagem personagem)
	{
		this.personagem = personagem;
	}
	
	@Override
	public void executar()
	{
		personagem.cessarTiro();
	}

}
