package command;

/**
 * @author Jadson Almeida
 * Created @d14 de mai de 2018
 */
public class Idle implements ComandoJogador
{
	private Personagem personagem;
	
	public Idle(Personagem personagem)
	{
		this.personagem = personagem;
	}
	
	@Override
	public void executar()
	{
		personagem.idle();
	}
}
