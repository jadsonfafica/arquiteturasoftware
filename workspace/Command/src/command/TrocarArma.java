package command;

/**
 * @author Jadson Almeida
 * Created @d14 de mai de 2018
 */
public class TrocarArma implements ComandoJogador
{
	private Personagem personagem;
	
	public TrocarArma(Personagem personagem)
	{
		this.personagem = personagem;
	}
	
	@Override
	public void executar()
	{
		personagem.proximaArma();
	}

}
