package command;

/**
 * @author Jadson Almeida
 * Created @d14 de mai de 2018
 */
public class Pular implements ComandoJogador
{
	private Personagem personagem;
	
	public Pular(Personagem personagem)
	{
		this.personagem = personagem;
	}
	
	@Override
	public void executar()
	{
		personagem.pular();
	}
}
