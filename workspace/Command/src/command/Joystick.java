package command;

/** @author Jadson Almeida Created @d14 de mai de 2018 Essa � a classe Invoker */
public class Joystick
{
	private ComandoJogador[] comandosFazer;
	private ComandoJogador[] comandosDesfazer;
	private ComandoJogador nada;

	public Joystick()
	{
		comandosFazer = new ComandoJogador[8]; // s�o 5 tipos de comados
		comandosDesfazer = new ComandoJogador[8]; // idem
		nada = new Nada();
		for (int i = 0; i < comandosFazer.length; i++)
		{
			comandosFazer[i] = nada;
			comandosDesfazer[i] = nada;
		}
	}

	public void setComando(int botao, ComandoJogador fazer, ComandoJogador desfazer)
	{
		comandosFazer[botao] = fazer;
		comandosDesfazer[botao] = desfazer;
	}

	public void apertarBotao(int botao)
	{
		comandosFazer[botao].executar();
	}

	public void soltarBotao(int botao)
	{
		comandosDesfazer[botao].executar();
	}
}
