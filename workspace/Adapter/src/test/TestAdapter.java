/*******************************************************************************
 * This Source Code is private for the autor's students.
 * Can't be readed to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies without autor Permission.
 * 
 * Autor: Jadson Almeida
 * Created: 26/03/2018
 ******************************************************************************/
package test;

import adapter.GameController;
import adapter.GameWindow;
import adapter.KeyboardInputAdaptee;
import adapter.MouseInput;

public class TestAdapter
{
	/** Cria um controlador do Game e adiciona um ouvidor de teclado adaptado para trabalhar
	 * como mouse */
	public static void main(String[] args)
	{
		// APP
		GameWindow gameWindow = new GameWindow();
		GameController game = new GameController(gameWindow);
		
		// Controlador do mouse
		game.getGameWindow().addMouseListener(new MouseInput(game));
		
		// Adapta��o do controlador do teclado
//		 game.getGameWindow().addKeyListener(new KeyboardInputAdaptee(new MouseInput(game), gameWindow));
	}
}
