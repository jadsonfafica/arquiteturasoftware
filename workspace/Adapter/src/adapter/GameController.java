/*******************************************************************************
 * This Source Code is private for the autor's students.
 * Can't be readed to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies without autor Permission.
 * 
 * Autor: Jadson Almeida
 * Created: 26/03/2018
 ******************************************************************************/
package adapter;

public class GameController 
{
	private GameWindow gameWindow;

	public GameController(GameWindow gameWindow)
	{
		this.gameWindow = gameWindow;
	}

	public void setPlayerPosition(int x, int y)
	{
		gameWindow.setPlayerPosition(x, y);
	}

	public GameWindow getGameWindow()
	{
		return gameWindow;
	}
}