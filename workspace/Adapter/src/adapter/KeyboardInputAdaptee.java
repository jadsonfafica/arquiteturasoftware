/*******************************************************************************
 * This Source Code is private for the autor's students.
 * Can't be readed to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies without autor Permission.
 * 
 * Autor: Jadson Almeida
 * Created: 26/03/2018
 ******************************************************************************/
package adapter;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

/** controlador de teclado que emula inputs do mouse para o game */
public class KeyboardInputAdaptee extends KeyboardInput
{
	/** controlador de mouse a ser emulado */
	private MouseInput mouse;
	/** controlador de game a ser atribuido os inputs */
	private GameWindow gameWindow; // necess�rio para criar o evento do mouse

	/** recebe um {@code MouseInput} que emular� os inputs desse teclado, e o game a ser
	 * controlado pelo input.
	 * @param mouse mouse a ser emulado
	 * @param gameWindow game a ser controlado */
	public KeyboardInputAdaptee(MouseInput mouse, GameWindow gameWindow)
	{
		this.mouse = mouse;
		this.gameWindow = gameWindow;
		x = gameWindow.getPlayerPosition().x;
		y = gameWindow.getPlayerPosition().y;
	}

	/** m�todo nativo que 'ouve' os inputs do teclado, sendo sobreescrito para detectar os
	 * inputs cl�ssicos do WASD. � criado um {@code MouseEvent} com os inputs capturados,
	 * emulando um input de mouse */
	@Override
	public void keyPressed(KeyEvent e)
	{
		super.keyPressed(e);
		mouse.mousePressed(new MouseEvent(gameWindow, 0, 0, 0, x, y, 1, true));
	}
}
