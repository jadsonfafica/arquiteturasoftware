/*******************************************************************************
 * This Source Code is private for the autor's students.
 * Can't be readed to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies without autor Permission.
 * 
 * Autor: Jadson Almeida
 * Created: 26/03/2018
 ******************************************************************************/
package adapter;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/** classe base de controlar inputs de teclado, nao adaptada ao Game */
public class KeyboardInput implements KeyListener
{
	/** posi��o inicial do personagem na tela */
	protected int x, y = 300;

	/** o m�todo n�o adaptado de controle de inputs */
	@Override
	public void keyPressed(KeyEvent e)
	{
		int command = e.getKeyCode();
		if (command == KeyEvent.VK_A)
		{
			x -= 10;
		} else if (command == KeyEvent.VK_D)
		{
			x += 10;
		} else if (command == KeyEvent.VK_W)
		{
			y -= 10;
		} else if (command == KeyEvent.VK_S)
		{
			y += 10;
		} else
		{
			return;
		}
	}

	@Override
	public void keyReleased(KeyEvent e)
	{
	}

	@Override
	public void keyTyped(KeyEvent e)
	{
	}

}
