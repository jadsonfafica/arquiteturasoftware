/*******************************************************************************
 * This Source Code is private for the autor's students.
 * Can't be readed to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies without autor Permission.
 * 
 * Autor: Jadson Almeida
 * Created: 26/03/2018
 ******************************************************************************/
package adapter;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/** controlador de inputs de mouse do game */
public class MouseInput implements MouseListener
{
	/** controlador de game a ser atribuido os inputs */
	private GameController game;

	/** recebe o game que ouvir� os inputs
	 * @param game game que ouvira os inputs */
	public MouseInput(GameController game)
	{
		this.game = game;
	}

	public void mousePressed(MouseEvent e)
	{
		int x = e.getX();
		int y = e.getY();
		game.setPlayerPosition(x, y);
	}

	public void mouseReleased(MouseEvent e)
	{
	}

	public void mouseEntered(MouseEvent e)
	{
	}

	public void mouseExited(MouseEvent e)
	{
	}

	public void mouseClicked(MouseEvent e)
	{
	}
}
