/**
 * App.java
 * @author Jadson Almeida
 * Created @d12 de abr de 2018
 */
package app;

import exception.NullPointerExceptionCustom;

public class App
{
	public static void main(String[] args)
	{
		try
		{
			Pessoa pessoa = new Pessoa(null);
			pessoa.getNome();
			System.out.println("funcionou");
		} catch (NullPointerExceptionCustom e)
		{
			System.out.println("Com exception: ok.");
		}
		Pessoa pessoa2 = new Pessoa(null);
		pessoa2.getNomeSemErro();
		System.out.println("Sem exception: ok.");
	}
}
