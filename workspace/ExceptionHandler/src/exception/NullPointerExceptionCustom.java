/**
 * NullPointerExceptionCustom.java
 * @author Jadson Almeida
 * Created @d12 de abr de 2018
 */
package exception;

import javax.swing.JOptionPane;

/** Essa classe cria � uma vers�o customizada da classe {@code NullPointException}. Ela cria
 * uma janela de Warning avisando da exce��o. */
public class NullPointerExceptionCustom extends NullPointerException
{
	/** Serial gerado aleatoriamente */
	private static final long serialVersionUID = -2389595548367089954L;

	/**  */
	public NullPointerExceptionCustom(String mensagem)
	{
		super(mensagem);
		JOptionPane.showMessageDialog(null, mensagem, "Erro inexperado",
				JOptionPane.ERROR_MESSAGE);
	}
}
