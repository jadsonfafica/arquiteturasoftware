package app;

import decorator.*;

public class Test
{
	public static void main(String[] args)
	{
		// simple melee physical attack with 100 pts
		Attack attackPhysical = new AttackPhysical(new AttackMelee(), 100);
		attackPhysical.dealAttack();
		System.out.println("----------");
		// simple ranged magical attack with 200 pts
		Attack attackMagical = new AttackMagical(new AttackRanged(), 200);
		attackMagical.dealAttack();
		System.out.println("----------");
		// simple ranged true attack with 50 pts
		Attack attackTrue = new AttackTrue(new AttackRanged(), 50);
		attackTrue.dealAttack();
		System.out.println("----------");
		// melee physical attack with slow and 185 pts
		Attack attackSlow = new AttackSlow(new AttackPhysical(new AttackMelee(), 185));
		attackSlow.dealAttack();
		System.out.println("----------");
		// ranged magic attack with stun and 200 pts
		Attack attackStun = new AttackStun(attackMagical);
		attackStun.dealAttack();
		System.out.println("----------");
		// fucking ranged true damage attack with 1000 pts 
		// and stun attack and magical damage of 2000 pts
		Attack attackOfNewChamp = new AttackStun(
				new AttackMagical(new AttackTrue(new AttackRanged(), 1000), 2000));
		attackOfNewChamp.dealAttack();
	}
}
