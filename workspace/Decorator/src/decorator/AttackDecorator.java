package decorator;

public class AttackDecorator implements Attack
{
	protected Attack attack;
	
	public AttackDecorator(Attack attack)
	{
		this.attack = attack;
	}
	
	@Override
	public void dealAttack()
	{
		attack.dealAttack();
	}
}
