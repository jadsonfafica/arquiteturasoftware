package decorator;

public class AttackMagical extends AttackDecorator
{
	protected int damage;
	
	public AttackMagical(Attack attack, int damage)
	{
		super(attack);
		this.damage = damage;
	}

	@Override
	public void dealAttack()
	{
		attack.dealAttack();
		magicalDamage();
	}
	
	private void magicalDamage() {
		System.out.println("This attack deal " + damage + " of MAGIC damage.");
	}
}