package decorator;

public class AttackRanged implements Attack
{
	@Override
	public void dealAttack()
	{
		System.out.println("This is a RANGED attack.");
	}
}
