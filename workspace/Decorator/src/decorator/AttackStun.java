package decorator;

public class AttackStun extends AttackDecorator
{
	public AttackStun(Attack attack)
	{
		super(attack);
	}

	@Override
	public void dealAttack()
	{
		attack.dealAttack();
		stun();
	}

	private void stun()
	{
		System.out.println("The target are STUNNED.");
	}
}
