package decorator;

public class AttackMelee implements Attack
{
	@Override
	public void dealAttack()
	{
		System.out.println("This is a MELEE attack.");
	}
}
