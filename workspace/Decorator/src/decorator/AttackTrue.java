package decorator;

public class AttackTrue extends AttackDecorator
{
	protected int damage;
	
	public AttackTrue(Attack attack, int damage)
	{
		super(attack);
		this.damage = damage;
	}

	@Override
	public void dealAttack()
	{
		attack.dealAttack();
		trueDamage();
	}
	
	private void trueDamage() {
		System.out.println("This attack deal " + damage + " of TRUE damage.");
	}
}
