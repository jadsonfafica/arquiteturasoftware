package decorator;

public interface Attack
{	
	public void dealAttack();
}
