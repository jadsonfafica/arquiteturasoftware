package decorator;

public class AttackSlow extends AttackDecorator
{
	public AttackSlow(Attack attack)
	{
		super(attack);
	}

	@Override
	public void dealAttack()
	{
		attack.dealAttack();
		slow();
	}

	private void slow()
	{
		System.out.println("The target are SLOWLY.");
	}
}
