package decorator;

public class AttackPhysical extends AttackDecorator
{
	protected int damage;
	
	public AttackPhysical(Attack attack, int damage)
	{
		super(attack);
		this.damage = damage;
	}

	@Override
	public void dealAttack()
	{
		attack.dealAttack();
		physicalDamage();
	}
	
	private void physicalDamage() {
		System.out.println("This attack deal " + damage + " of PHYSIC damage.");
	}
}
