package perfil;

import java.awt.Image;

/**
 * @author Jadson Almeida
 * Created @d19 de abr de 2018
 */
public class Perfil implements PerfilPesquisavel
{
	private Image imagem;
	private String nome;
	private String descricao;
	// um monte de outras variaveis
	
	public Perfil (Image imagem, String nome, String descricao)
	{
		this.imagem = imagem;
		this.nome = nome;
		this.descricao = descricao;
	}
	
	@Override
	public Image getImagem()
	{
		System.out.println("Exibe a imagem do perfil");
		return imagem;
	}

	@Override
	public String getNome()
	{
		return nome;
	}

	@Override
	public String getDescricaoResumida()
	{
		return descricao;
	}

	// um monte de outros m�todos
}
