package perfil;

import java.awt.Image;

/**
 * @author Jadson Almeida
 * Created @d19 de abr de 2018
 * Esta classe � um proxy para as que implementam PerfilPesquisavel
 */
public class PerfilProxy implements PerfilPesquisavel
{
	/** PerfilPesquisavel a ser controlado */
	private PerfilPesquisavel perfilPesquisavel;
	
	public PerfilProxy(PerfilPesquisavel perfilPesquisavel)
	{
		this.perfilPesquisavel = perfilPesquisavel;
	}
	
	@Override
	public Image getImagem()
	{
		System.out.println("Exibir anuncio");
		return perfilPesquisavel.getImagem();
	}

	@Override
	public String getNome()
	{
		return perfilPesquisavel.getNome();
	}

	@Override
	public String getDescricaoResumida()
	{
		return perfilPesquisavel.getDescricaoResumida();
	}

}
