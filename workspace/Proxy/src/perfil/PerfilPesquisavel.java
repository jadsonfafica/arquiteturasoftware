package perfil;

import java.awt.Image;

/** @author Jadson Almeida Created @d19 de abr de 2018 Simula a interface de uma busca por
 *         perfil no facebook */
public interface PerfilPesquisavel
{
	/** retorna a imagem do perfil */
	public Image getImagem();

	/** retorna o nome do perfil */
	public String getNome();

	/** retorna a descrição contida na "Apresentação" do perfil */
	public String getDescricaoResumida();
}
