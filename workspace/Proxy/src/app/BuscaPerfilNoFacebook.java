package app;

import perfil.Perfil;
import perfil.PerfilPesquisavel;
import perfil.PerfilProxy;

/** @author Jadson Almeida Created @d19 de abr de 2018 Cont�m metodo main que simula uma busca
 *         por perfil no Facebook */
public class BuscaPerfilNoFacebook
{
	public static void main(String[] args)
	{
		PerfilPesquisavel perfilResumido = new PerfilProxy(new Perfil(null, "Jadson Almeida",
				"Trabalha na FAFICA\nGamer\nMora em Caruaru"));
		perfilResumido.getImagem();
		System.out.println(perfilResumido.getNome());
		System.out.println(perfilResumido.getDescricaoResumida());
	}
}
