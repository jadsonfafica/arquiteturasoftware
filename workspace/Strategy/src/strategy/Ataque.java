package strategy;

/** @author Jadson Almeida Created @d3 de jun de 2018. Essa classe � uma estrat�gia */
public interface Ataque
{
	// personagem = quem est� atirando
	public void atacar(Personagem personagem);
}
