package strategy;

/** @author Jadson Almeida Created @d4 de jun de 2018. Essa classe � uma Concrete Strategy */
public class Awm implements Ataque
{
	@Override
	public void atacar(Personagem personagem)
	{
		System.out.println("atira uma bala de alto impacto com grande distancia");
	}
}
