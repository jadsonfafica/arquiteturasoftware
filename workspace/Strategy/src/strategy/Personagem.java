package strategy;

/** @author Jadson Almeida Created @d4 de jun de 2018. Essa classe � o contexto. */
public class Personagem
{
	/** Tipo de ataque do personagem */
	private Ataque ataque;

	public Personagem(Ataque ataque)
	{
		this.ataque = ataque;
	}

	/** o personagem ativa um ataque */
	public void atacar()
	{
		ataque.atacar(this);
	}

	/** muda o tipo de ataque do personagem */
	public void setAtaque(Ataque ataque)
	{
		this.ataque = ataque;
	}
}
