package strategy;

/** @author Jadson Almeida Created @d4 de jun de 2018. Essa classe � uma Concrete Strategy */
public class Faca implements Ataque
{
	@Override
	public void atacar(Personagem personagem)
	{
		System.out.println("golpeia com a l�mina alvos at� 1.5 metros de distancia");
	}
}
