package app;

import strategy.*;

/** @author Jadson Almeida Created @d4 de jun de 2018. Essa classe teste o padr�o de projetos
 *         Strategy. */
public class Teste // demo
{
	public static void main(String[] args)
	{
		Personagem personagem = new Personagem(new Faca());
		personagem.atacar();
		personagem.setAtaque(new Awm());
		personagem.atacar();
		personagem.setAtaque(new M416());
		personagem.atacar();
	}
}
