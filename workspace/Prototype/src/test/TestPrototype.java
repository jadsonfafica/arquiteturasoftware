/*******************************************************************************
 * This Source Code is private for the autor's students.
 * Can't be readed to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies without autor Permission.
 * 
 * Autor: Jadson Almeida
 * Created: 26/03/2018
 ******************************************************************************/
package test;

import java.awt.geom.Point2D;
import java.util.Random;

import Prototype.Monster;

/** Test the Prototype pattern creating 3 differents Monsters and clonning them */
public class TestPrototype
{
	/** Create 3 types of Monsters and call SpawnMonster of each one */
	public static void main(String[] args)
	{
		// create 3 monsters type
		Monster goblin = new Monster(1l, "Goblin", (byte) 5, (byte) 2, (byte) 10, (short) 100);
		Monster skeletonArchery = new Monster(2l, "Skeleton Archery", (byte) 15, (byte) 5,
				(byte) 6, (short) 80);
		Monster ogre = new Monster(3l, "Ogre", (byte) 25, (byte) 2, (byte) 10, (short) 500);
		// Clone and spawn monsters goblin
		CloneAndSpawnMonster(goblin, 5);
		CloneAndSpawnMonster(skeletonArchery, 5);
		CloneAndSpawnMonster(ogre, 3);
	}

	/** Spawn monster in a random coordinate (x,y) between 0 and 1000 */
	/** @param monster The monster to spawn */
	/** @param amount Amount of this monster must be spawned in different coordinates */
	public static void CloneAndSpawnMonster(Monster monster, int amount)
	{
		Random rnd = new Random();
		Point2D position = new Point2D.Float(0, 0);
		for (int i = 0; i < amount; i++)
		{
			// Generate x and y between 0 and 1000
			float x = rnd.nextFloat() * 1000;
			float y = rnd.nextFloat() * 1000;
			// Set the position to spawn
			position.setLocation(x, y);
			// Clone a monster 
			Monster m = new Monster(monster);
			SpawnMonster(monster, position);
		}
	}
	
	static void SpawnMonster(Monster monster, Point2D position) {
		System.out.println("Spawning " + monster.getName() + 
				" in the position " + position);
	}
}
